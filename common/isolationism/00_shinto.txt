# In-game the entries are (stable) sorted by isolation_value (ascending) and mapped to 0-[number of entries - 1] levels. Negative values are not considered valid.

suraels_voice_isolation = {
	isolation_value = 0

	modifier = {
		manpower_recovery_speed = 0.20
		idea_cost = -0.10
	}
}

suraels_light_isolation = {
	isolation_value = 1

	modifier = {
		advisor_cost = -0.10
		leader_cost = -0.10
	}
}

suraels_wisdom_isolation = {
	isolation_value = 2

	modifier = {
		adm_tech_cost_modifier = -0.05
		trade_efficiency = 0.10
	}
}

suraels_authority_isolation = {
	isolation_value = 3

	modifier = {
		discipline = 0.025
		legitimacy = 1
	}
}

suraels_incarnation_isolation = {
	isolation_value = 4

	modifier = {
		stability_cost_modifier = -0.15
		global_unrest = -2
	}
}

closed_runelink = {
	isolation_value = 5
	
	modifier = {
		global_unrest = 4
		global_tax_modifier = 0.2
		all_power_cost = 0.05
	}
}

weak_runelink = {
	isolation_value = 6
	
	modifier = {
		global_tax_modifier = -0.05
		global_unrest = 2
		development_cost = -0.025
		global_manpower_modifier = 0.05
	}

}
stable_runelink = {
	isolation_value = 7
	
	modifier = {
		global_manpower_modifier = 0.1
		development_cost = -0.05
		global_tax_modifier = -0.1
	}
}

strong_runelink = {
	isolation_value = 8
	
	modifier = {
		global_manpower_modifier = 0.15
		development_cost = -0.075
		global_tax_modifier = -0.15
	}
}

powerful_runelink = {
	isolation_value = 9
	
	modifier = {
		global_manpower_modifier = 0.2
		development_cost = -0.1
		global_tax_modifier = -0.2
	}
}
